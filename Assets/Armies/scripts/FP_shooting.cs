﻿using UnityEngine;
using System.Collections;

public class FP_shooting : MonoBehaviour {

	public GameObject bullet_prefab;

	float bulletSpeed = 20.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1") && Camera.main.name == "Main Camera") {
			Camera cam = Camera.main;
			GameObject bullet = (GameObject)Instantiate(bullet_prefab, cam.transform.position, cam.transform.rotation);
			bullet.rigidbody.AddForce(cam.transform.forward * bulletSpeed, ForceMode.Impulse);
		}
	}
}
