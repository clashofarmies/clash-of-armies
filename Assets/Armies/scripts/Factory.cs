﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Factory : MonoBehaviour {
	
	private delegate void Menu(); 
	private Menu menu;

	public GameObject unit1;
	public int TrainingTime1 = 5;
	public GameObject unit2;
	public int TrainingTime2 = 4;
	public int Capacity = 5;
	private Transform Spawnplace;
	
	private bool IsTraining = false;
	private List<int> trainingTimesList = new List<int>();
	private List<GameObject> trainingUnitsList = new List<GameObject>();
	private int TimeToTrain;
	private GameObject UnitToTrain;
	private int ButtonWidth = 100;
	
	void Awake(){
		this.menu = MenuOff;
		Spawnplace = transform.FindChild ("Spawnplace").transform;
	}
	
	public void Start(){}
	void Update () {
		if ((IsTraining == false) & (trainingUnitsList.Count > 0)) {
			StartCoroutine("MakeUnit");
		}
	}
	
	public void OnMouseDown()
	{
		if (this.menu == MenuOff) {
			this.menu = MenuOn;
		}
		else if (this.menu == MenuOn) {
			this.menu = MenuOff;
		}
	}
	public void OnGUI(){
		this.menu ();
	}
	
	public void MenuOff(){}
	public void MenuOn(){
		if (Input.GetKeyDown ("1")){
			Button1();
		}
		if (Input.GetKeyDown ("2")){
			Button2 ();
		}
		if (Input.GetKeyDown ("x")) {
			ExitButton();		
		}
		if (GUI.Button (new Rect (10, 10, ButtonWidth, 20), "Unit 1" + "(" + TrainingTime1.ToString() + "s)")) {
			Button1();
		}
		if (GUI.Button (new Rect (10, 40, ButtonWidth, 20), "Unit 2" + "(" + TrainingTime2.ToString() + "s)")) {
			Button2();
		}
		if (GUI.Button (new Rect (10, 70, ButtonWidth, 20), "Exit (x)")){
			ExitButton();
		}
		if (trainingUnitsList.Count > 0) {
			UnderTrainingButton();
		}
		if (trainingUnitsList.Count > 1) {
			TrainingListButton();
		}
	}
	void Button1(){
		if (trainingUnitsList.Count < Capacity) {
			trainingTimesList.Add (TrainingTime1);
			trainingUnitsList.Add (unit1);
		}
	}
	void Button2(){
		if (trainingUnitsList.Count < Capacity) { 
			trainingTimesList.Add (TrainingTime2);
			trainingUnitsList.Add (unit2);
		}
	}
	void ExitButton(){
		this.menu = MenuOff;
	}
	void UnderTrainingButton(){
		if (GUI.Button (new Rect (10, 100, ButtonWidth, 20), trainingTimesList [0].ToString ())) {
			StopCoroutine("MakeUnit");
			trainingTimesList.RemoveAt(0);
			trainingUnitsList.RemoveAt(0);
			IsTraining = false;
		}
	}
	void TrainingListButton(){
		for (int i = 1; i < trainingUnitsList.Count; i++){
			if (GUI.Button(new Rect (10+ButtonWidth*i, 100, ButtonWidth, 20), trainingTimesList[i].ToString())){
				trainingTimesList.RemoveAt(i);
				trainingUnitsList.RemoveAt(i);
			}
		}
	}
	
	IEnumerator MakeUnit(){
		IsTraining = true;
		TimeToTrain = trainingTimesList [0];
		UnitToTrain = trainingUnitsList [0];
		//play any effects, animations, etc.
		yield return new WaitForSeconds (TimeToTrain);
		Instantiate(UnitToTrain, Spawnplace.position, Spawnplace.rotation);
		trainingTimesList.RemoveAt (0);
		trainingUnitsList.RemoveAt (0);
		IsTraining = false;
	}
}
