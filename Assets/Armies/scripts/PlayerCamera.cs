﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public Transform target;

	public float targetHeight = 5.0f;
	public float distance = 10.0f;
	
	public float maxDistance = 20.0f;
	public float minDistance = 2.5f;
	
	public float xSpeed = 150.0f;
	public float ySpeed = 70.0f;
	
	public float yMinLimit = -65.0f;
	public float yMaxLimit = 65.0f;
	
	public float zoomRate = 20.0f;

	float x = 0.0f;
	float y = 0.0f;

	public void SetTarget(Transform newTarget) {
		this.target = newTarget;
	}

	// Use this for initialization
	void Start () {
		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (!target) {
			return;
		}

		x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
		y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

		distance -= (Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime) * zoomRate * Mathf.Abs(distance);
		distance = Mathf.Clamp(distance, minDistance, maxDistance);
		
		y = ClampAngle(y, yMinLimit, yMaxLimit);

		Quaternion rotation = Quaternion.Euler(y, x, 0);
		Vector3 position = target.position - (
			rotation * Vector3.forward * distance + 
			new Vector3(0,-targetHeight,0)
		);
		
		transform.rotation = rotation;
		transform.position = position;

	}

	float ClampAngle ( float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}

}
