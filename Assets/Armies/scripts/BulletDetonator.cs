﻿using UnityEngine;
using System.Collections;

public class BulletDetonator : MonoBehaviour {

	float lifespan = 3.0f;
	public float damage = 40.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		lifespan -= Time.deltaTime;
		if (lifespan <= 0) {
			Expolde ();
		}
	}

	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Enemy") {
			Destroy (collision.gameObject);
			Destroy (gameObject);
		}
	}

	void Expolde() {
		Destroy (gameObject);

	}
}
