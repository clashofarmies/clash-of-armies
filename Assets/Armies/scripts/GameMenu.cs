﻿using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {

	public bool TeamA = false;
	public bool TeamB = false;
	public bool isQuit = false;
	static public int Team = 1;  

	void OnMouseEnter () {
		guiText.material.color = Color.red;
	}

	void OnMouseExit() {
		guiText.material.color = Color.white;
	}

	void OnMouseUp() {
		if (isQuit) {
			Application.Quit();
		}
		if (TeamA) {
			Team = 0;
		}

		if (TeamB) {
			Team = 1;
		}
		Application.LoadLevel(1);

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit();
		}
	}
}
