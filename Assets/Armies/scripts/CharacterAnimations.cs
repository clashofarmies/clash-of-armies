﻿using UnityEngine;
using System.Collections;

public class CharacterAnimations : MonoBehaviour {
		
	public float jumpSpeed = 8.0f;
	public float gravity = 20.0f;
	public float runSpeed = 25.0f;
	public float walkSpeed = 15.0f;
	public float rotateSpeed = 150.0f;

	public AnimationClip run;
	public AnimationClip idleattack;
	public AnimationClip death;
	public AnimationClip attack;
	public AnimationClip jump;
	public AnimationClip victory;

	bool grounded = false;
	string moveStatus = "idle";
	bool jumping = false;
	float moveSpeed = 0.0f;
	Vector3 moveDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetMoveStatus(string status) {
		moveStatus = status;
	}
	public void PlayAnims(GameObject character) {

		// Only allow movement and jumps while grounded
		if(grounded) {
			moveDirection = new Vector3( Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
			
			// if moving forward and to the side at the same time, compensate for distance
			if(Input.GetAxis("Horizontal") != 0 && Input.GetAxis("Vertical") != 0) {
				moveDirection *= .7f;
			}
			
			moveDirection = transform.TransformDirection(moveDirection);
			moveDirection *= runSpeed;
			
			if (moveStatus != "attack") {
				moveStatus = "idle";
			}
			if(moveDirection != Vector3.zero)
				moveStatus = "running";
			
			// Jump!
			if(Input.GetButton("Jump")) {
				moveDirection.y = jumpSpeed;
				moveStatus = "jump";
			}
		}
		
		if (Input.GetButtonDown ("Fire1") && Camera.main.name == "Main Camera" && !character.animation.IsPlaying("attack")) {
			moveStatus = "attack";
		}

		if (moveStatus == "attack") {
			character.animation [attack.name].speed = character.animation [attack.name].length / 1f;
			character.animation.CrossFade (run.name, 0.12f);
			character.animation.Play( "attack" );
			StartCoroutine(WaitForAnimation(character.animation [attack.name].length));
		}
		
		if (moveStatus == "running") {
			character.animation [run.name].speed = character.animation [run.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("run");
		}
		
		if (moveStatus == "idle") {
			character.animation [idleattack.name].speed = character.animation [idleattack.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("idlebattle");
		}
		
		if (moveStatus == "jump") {
			character.animation [jump.name].speed = character.animation [jump.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("jump");
		}
	}
	
	IEnumerator WaitForAnimation(float waitTime){
		yield return new WaitForSeconds(waitTime);
		moveStatus = "idle";
	}
	
	float GetSpeed () {
		if (moveStatus == "idle")
			moveSpeed = 0;
		if (moveStatus == "walking")
			moveSpeed = walkSpeed;
		if (moveStatus == "running")
			moveSpeed = runSpeed;
		return moveSpeed;
	}
	
	bool IsJumping () {
		return jumping;
	}
	


}
