﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]
[RequireComponent (typeof(CharacterController))]
public class FPcontroller : MonoBehaviour {

	public float jumpSpeed = 8.0f;
	public float gravity = 20.0f;
	public float runSpeed = 25.0f;
	public float rotateSpeed = 150.0f;
	public float maxVelocityChange = 10.0f;
	public bool canJump = true;
	public float jumpHeight = 2.0f;

	public AnimationClip run;
	public AnimationClip idleattack;
	public AnimationClip death;
	public AnimationClip attack;
	public AnimationClip jump;
	public AnimationClip victory;
	
	bool grounded = false;
	bool isAttack = false;
	string moveStatus = "idle";
	bool jumping = false;
	float moveSpeed = 0.0f;

	GameObject character = null;

	CharacterController characterController;

	// Use this for initialization
	void Start () {
		character = gameObject.transform.Find("Soldier").gameObject;
		
		run = character.animation.GetClip("run");
		idleattack = character.animation.GetClip("idlebattle");
		attack = character.animation.GetClip("attack");
		jump = character.animation.GetClip("jump");
		death = character.animation.GetClip("die");
		victory = character.animation.GetClip("victory");

	}

	void Awake () {
		rigidbody.freezeRotation = true;
		rigidbody.useGravity = false;
	}

	void FixedUpdate () {

		ControllMove ();
		
		if (Input.GetButtonDown ("Fire1") && Camera.main.name == "Main Camera" && !character.animation.IsPlaying("attack")) {
			moveStatus = "attack";
			AttackClosestEnemy();
		}

		if (!isAttack) {
			PlayAnims (character);
		}
		
	}

	GameObject AttackClosestEnemy() {
		float radius = 5.0f;

		Collider[] colliders = Physics.OverlapSphere (transform.position, radius);
		Collider closestCollider = null;
		foreach (Collider hit in colliders) {
			//checks if it's hitting itself
			if(hit.collider == transform.collider) {
				continue;
			}
			if(!closestCollider) {
				closestCollider = hit;
			}
			//compares distances
			if(Vector3.Distance(transform.position, hit.transform.position) <= Vector3.Distance(transform.position, closestCollider.transform.position))
			{
				closestCollider = hit;
			}
		}

		if (closestCollider.tag == "character") {
			closestCollider.gameObject.GetComponent<ArmiesHealth> ().DecreaseHealth(50) ;
		}

		return closestCollider.gameObject;
	}

	void ControllMove() {

		if (grounded) {
			// Calculate how fast we should be moving
			Vector3 targetVelocity = new Vector3 (Input.GetAxis ("Horizontal"), 0, Input.GetAxis ("Vertical"));
			targetVelocity = transform.TransformDirection (targetVelocity);
			targetVelocity *= runSpeed;
			
			// Apply a force that attempts to reach our target velocity
			Vector3 velocity = rigidbody.velocity;
			Vector3 velocityChange = (targetVelocity - velocity);
			velocityChange.x = Mathf.Clamp (velocityChange.x, -maxVelocityChange, maxVelocityChange);
			velocityChange.z = Mathf.Clamp (velocityChange.z, -maxVelocityChange, maxVelocityChange);
			velocityChange.y = 0;
			rigidbody.AddForce (velocityChange, ForceMode.VelocityChange);
			
			if (moveStatus != "attack") {
				moveStatus = "idle";
			}
			if(targetVelocity != Vector3.zero)
				moveStatus = "running";
			
			// Jump!
			if(Input.GetButton("Jump")) {
				moveStatus = "jump";
			}
			
			// Jump
			if (canJump && Input.GetButton ("Jump")) {
				rigidbody.velocity = new Vector3 (velocity.x, CalculateJumpVerticalSpeed (), velocity.z);
			}
		}
		
		// We apply gravity manually for more tuning control
		rigidbody.AddForce (new Vector3 (0, -gravity * rigidbody.mass, 0));
		transform.rotation = Quaternion.Euler(0,Camera.main.transform.eulerAngles.y,0);
	
		grounded = false;
	}

	float CalculateJumpVerticalSpeed () {
		// From the jump height and gravity we deduce the upwards speed 
		// for the character to reach at the apex.
		return Mathf.Sqrt(2 * jumpHeight * gravity);
	}

	void OnCollisionStay () {
		grounded = true;	
	}

	void PlayAnims(GameObject character) {
		if (moveStatus == "attack") {
			character.animation [attack.name].speed = character.animation [attack.name].length / 1f;
			character.animation.CrossFade (run.name, 0.12f);
			character.animation.Play( "attack" );
			isAttack = true;
			StartCoroutine(WaitForAnimation(character.animation [attack.name].length));
		}
		
		if (moveStatus == "running") {
			character.animation [run.name].speed = character.animation [run.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("run");
		}
		
		if (moveStatus == "idle") {
			character.animation [idleattack.name].speed = character.animation [idleattack.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("idlebattle");
		}
		
		if (moveStatus == "jump") {
			character.animation [jump.name].speed = character.animation [jump.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("jump");
		}
	}

	IEnumerator WaitForAnimation(float waitTime){
		yield return new WaitForSeconds(waitTime);
		isAttack = false;
		moveStatus = "idle";
	}
	
	float GetSpeed () {
		if (moveStatus == "idle")
			moveSpeed = 0;
		if (moveStatus == "running")
			moveSpeed = runSpeed;
		return moveSpeed;
	}
	
	bool IsJumping () {
		return jumping;
	}
	
}
