﻿using UnityEngine;
using System.Collections;

public class ArmiesHealth : MonoBehaviour {

	public float maxHealth = 100.0f;
	public float currentHealth = 100.0f;
	public float healthBarLength = 200.0f;

	GameObject own;
	GameObject Game;
	// Use this for initialization
	void Start () {
		Game = GameObject.Find("Game");
	}

	public void DecreaseHealth(float demage) {
		//Debug.Log ("currentHealth: " + currentHealth + " demage:" + demage);
		currentHealth -= demage;
		own = gameObject;

		if (currentHealth <= 0) {
			Game.GetComponent<GameDriver> ().ChangeEnemyChar(own);
		}
	}

	void OnGUI() {
		if (Camera.main.name != "Main Camera") {
			return;
		}

		Vector2 targetPos;		
		targetPos = Camera.main.WorldToScreenPoint (transform.position);
		
		GUI.Box(new Rect(targetPos.x, Screen.height - targetPos.y-150, 60, 20), currentHealth + "/" + maxHealth);

	}

}
