﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]
public class HumanAI : MonoBehaviour {
	public Transform target;
	public int rotationSpeed = 20;
	public int maxdistance = 100;


	public float jumpSpeed = 8.0f;
	public float gravity = 20.0f;
	public float runSpeed = 25.0f;
	public float rotateSpeed = 150.0f;
	public float maxVelocityChange = 10.0f;
	public bool canJump = true;
	public float jumpHeight = 2.0f;
	
	public AnimationClip run;
	public AnimationClip idleattack;
	public AnimationClip death;
	public AnimationClip attack;
	public AnimationClip jump;
	public AnimationClip victory;

	bool isWalking = false;
	bool isAttack = false;
	string moveStatus = "idle";
	bool jumping = false;
	float moveSpeed = 20.0f;
	Vector3 moveDirection = Vector3.zero;
	
	GameObject character = null;

	public void SetTarget(Transform newTarget) {
		this.target = newTarget;
	}
	
	private Transform myTransform;
	
	void Awake(){
		myTransform = transform;
	}

	void Start () {		
		maxdistance = 7;

		character = gameObject.transform.Find("Soldier").gameObject;
		
		run = character.animation.GetClip("run");
		idleattack = character.animation.GetClip("idlebattle");
		attack = character.animation.GetClip("attack");
		jump = character.animation.GetClip("jump");
		death = character.animation.GetClip("die");
		victory = character.animation.GetClip("victory");

	}
	
	void Update () {

		ControllMove ();

		if (!isAttack) {
			PlayAnims (character);
		}
		
	}

	void ControllMove() {

		myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(target.position - myTransform.position), rotationSpeed * Time.deltaTime);
		
		if(Vector3.Distance(target.position, myTransform.position) > maxdistance){
			moveDirection = new Vector3(myTransform.position.x,0,myTransform.position.y);
			
			// if moving forward and to the side at the same time, compensate for distance
			moveDirection *= .7f;
			//Move towards target
			myTransform.position += myTransform.forward * moveSpeed * Time.deltaTime;
			
		} 
		moveDirection = transform.TransformDirection(moveDirection);
		moveDirection *= runSpeed;	
		
		if (moveStatus != "attack") {
			moveStatus = "idle";
		}
		if(moveDirection != Vector3.zero)
			moveStatus = isWalking ? "walking" : "running";

		rigidbody.AddForce (new Vector3 (0, -gravity * rigidbody.mass, 0));

	}

	void PlayAnims(GameObject character) {
		if (moveStatus == "attack") {
			character.animation [attack.name].speed = character.animation [attack.name].length / 1f;
			character.animation.CrossFade (run.name, 0.12f);
			character.animation.Play( "attack" );
			isAttack = true;
			StartCoroutine(WaitForAnimation(character.animation [attack.name].length));
		}
		
		if (moveStatus == "running") {
			character.animation [run.name].speed = character.animation [run.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("run");
		}
		
		if (moveStatus == "idle") {
			character.animation [idleattack.name].speed = character.animation [idleattack.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("idlebattle");
		}
		
		if (moveStatus == "jump") {
			character.animation [jump.name].speed = character.animation [jump.name].length / 0.8f;
			character.animation.CrossFade (run.name, 0.12f);	
			character.animation.Play ("jump");
		}
	}
	
	IEnumerator WaitForAnimation(float waitTime){
		yield return new WaitForSeconds(waitTime);
		isAttack = false;
		moveStatus = "idle";
	}
	
	float GetSpeed () {
		if (moveStatus == "idle")
			moveSpeed = 0;
		if (moveStatus == "running")
			moveSpeed = runSpeed;
		return moveSpeed;
	}
	
	bool IsJumping () {
		return jumping;
	}
}
