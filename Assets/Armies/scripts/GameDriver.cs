﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameDriver : MonoBehaviour {
		
	public GameObject human_soldier;

	GameObject FpsCamera;
	GameObject SCamera;

	int maxTeamNumber = 2;
	int numSoldiers = 26;
	int currentSoldier = 0;
	int currentEnemySoldier = 0;
	int EnemyTeam = 0;
	GameObject[][] Armies;
	GameObject[] SoldiersA;
	GameObject[] SoldiersB;
	GameObject ActiveEnemy = null;

	// Use this for initialization
	void Start () {		
		FpsCamera = GameObject.Find("Main Camera");
		FpsCamera.SetActive (false);
		SCamera = GameObject.Find("Strategy Camera");
		SCamera.SetActive (true);
		SoldiersA = new GameObject[numSoldiers];
		SoldiersB = new GameObject[numSoldiers];
		Armies = new GameObject[maxTeamNumber][];

		GameObject ArmyA = GameObject.Find("ArmyA");
		GameObject ArmyB = GameObject.Find("ArmyB");

		GenerateArmy (ArmyA, -150, SoldiersA);
		GenerateArmy (ArmyB, 30, SoldiersB);

		Armies[0] = SoldiersA;
		Armies[1] = SoldiersB;

		GameObject First = Armies[GameMenu.Team][currentSoldier];
		SetPlayerCamera (Armies[GameMenu.Team][currentSoldier]);


		if (GameMenu.Team == 0) {
			EnemyTeam = 1;
		}

		GameObject FirstEnemy = Armies[EnemyTeam][currentSoldier];
		SetFirstEnemy (FirstEnemy, First);
	}

	void GenerateArmy (GameObject Army, int zOffset, GameObject[] Collector)	{
		int index = 0;
		for (int z = 0; z < 5; z++) {
			for (int x = 0; x < 5; x++) {
				Collector [index] = (GameObject)Instantiate (human_soldier, new Vector3 (x + 2 * x, 0, z - 5 * z + zOffset), Quaternion.identity);
				Collector [index].transform.parent = Army.transform;
				index++;
			}
		}
	}

	public void SetPlayerCamera(GameObject FirstPlayerUnit) {
		FirstPlayerUnit.AddComponent<FPcontroller>();
		FirstPlayerUnit.AddComponent<ArmiesHealth>();
		FpsCamera.GetComponent<PlayerCamera>().SetTarget (FirstPlayerUnit.transform);
	}

	public void SetFirstEnemy(GameObject FirstEnemy, GameObject target) {
		FirstEnemy.AddComponent<ArmiesHealth>();
		FirstEnemy.AddComponent<HumanAI>();
		FirstEnemy.GetComponent<HumanAI> ().SetTarget (target.transform);
		
		ActiveEnemy = FirstEnemy;
	}

	public GameObject GetActiveEnemy() {
		return ActiveEnemy;
	}

	// Update is called once per frame
	void Update () {
		
		if (Input.GetKeyDown(KeyCode.C)) {
			FpsCamera.SetActive(!FpsCamera.activeSelf);
			SCamera.SetActive(!SCamera.activeSelf);
		}	

		if (FpsCamera.activeSelf == true) {
			Screen.lockCursor = true;
		} else {
			Screen.lockCursor = false;
		}
				
		if (Input.GetKeyDown(KeyCode.K)) {
			ChangeRTSChar();
		}

		if (Input.GetKeyDown (KeyCode.L)) {
			Application.Quit ();
		}

	}

	void ChangeRTSChar() {
		GameObject OldTarget = Armies[GameMenu.Team][currentSoldier];
		Debug.Log(OldTarget);
		Debug.Log(currentSoldier);
		currentSoldier += 1;
		GameObject NewTarget = Armies[GameMenu.Team][currentSoldier];
		
		NewTarget.AddComponent<FPcontroller>();
		FpsCamera.GetComponent<PlayerCamera>().SetTarget (NewTarget.transform);

		Armies[EnemyTeam] [currentEnemySoldier].GetComponent<HumanAI> ().SetTarget (NewTarget.transform);

		Destroy (OldTarget);
	}

	public void ChangeEnemyChar(GameObject OldTarget) {
		currentEnemySoldier += 1;

		GameObject NewEnemy = Armies[EnemyTeam] [currentEnemySoldier];

		NewEnemy.AddComponent<ArmiesHealth>();
		NewEnemy.AddComponent<HumanAI>();
		NewEnemy.GetComponent<HumanAI> ().SetTarget (Armies[GameMenu.Team][currentSoldier].transform);
		
		Destroy (OldTarget);
	}

}
